#!/bin/sh

if [ -z "$1" ]; then
	exit 1
fi

status=`docker inspect --format="{{.State.Running}}" xivocc-$1-1 2>/dev/null`

if [ "$status" = "true" ]; then
    exit 0
else
    exit 1
fi

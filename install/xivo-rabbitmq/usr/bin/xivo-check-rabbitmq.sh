#!/usr/bin/env bash

set -e

RETRIES=60

container_status=$(/usr/bin/container-check-xivo.sh rabbitmq ; echo $?)

if [ "$container_status" -ne 0 ]; then
    echo "ERROR: rabbitmq container is not running"
    exit 1
fi

for i in $(seq $RETRIES -1 1); do
    diagnostics_status=$(/usr/bin/xivo-dcomp exec -T rabbitmq "rabbitmq-diagnostics -q check_running" >/dev/null 2>&1 ; echo $?)

    if [ "$diagnostics_status" -eq 0 ]; then
        exit 0
    fi

    sleep 1
done

echo "ERROR: rabbitmq container is still not ready for amqp connection"
exit 1

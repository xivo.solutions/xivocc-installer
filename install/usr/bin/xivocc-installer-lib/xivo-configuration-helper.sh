#!/bin/bash

COMPOSE_PATH="/etc/docker/compose"
CUSTOM_ENV_FILE="custom.env"

INSTALLATION_TYPE="CC"
XIVOCC_CONFIG_SCRIPT="/usr/bin/xivocc-configure-xivopbx"
RESTART_REPLY=false

source_custom_env_file() {
    # shellcheck disable=SC1090
    source ${COMPOSE_PATH}/${CUSTOM_ENV_FILE}

    # Check if there's value, else, will add default
    XIVO_HOST=${XIVO_HOST:-127.0.0.1}
    XUC_HOST=${XUC_HOST:-127.0.0.1}
    CONFIGURE_REPLY=${CONFIGURE_REPLY:-false}
    WEEKS_TO_KEEP=${WEEKS_TO_KEEP:-52}
    RECORDING_WEEKS_TO_KEEP=${RECORDING_WEEKS_TO_KEEP:-52}
    RESTART_REPLY=${RESTART_REPLY:-false}
    USM_EVENT_AMQP_SECRET=${USM_EVENT_AMQP_SECRET:-default}
}

check_if_xivo_configured() {
    local xivo_host="${1}"
    local wizard_state
    wizard_state=$(curl -sk -X GET --header 'Accept: application/json' "https://${xivo_host}:9486/1.1/wizard")

    if echo "${wizard_state}" | grep -q '"configured"[[:space:]]*:[[:space:]]*true'; then
        echo -e "\e[1;32mYour Xivo is configured\e[0m"
        return 0
    else
        echo -e "\e[1;31mYour Xivo is not configured\e[0m"
        return 1
    fi
}

check_if_xivo_is_ssh_reachable() {
    local xivo_host="${1}"
    local ssh_state
    ssh -o PasswordAuthentication=no -o ConnectTimeout=3 "root@${xivo_host}" echo "Can reach XiVO under ${xivo_host}"
    ssh_state=$?

    if [ "$ssh_state" -eq 0 ]; then
        echo -e "\e[1;32mYour XivoCC can reach XiVO PBX via SSH\e[0m"
        return 0
    else
        echo -e "\e[1;31mYour XivoCC cannot reach XiVO PBX via SSH with ssh keys, check your SSH config\e[0m"
        return 1
    fi
}

install_package_on_xivo() {
    local xivo_host="${1}"
    ssh "root@${xivo_host}" 'DEBIAN_FRONTEND=noninteractive apt-get install --yes -o Dpkg::Options::="--force-confnew" xivocc-docker-components'
}

launch_xivo_configuration_script() {
    local xivo_host="${1}"
    echo -e "\e[1;33mXiVO PBX Configuration...\e[0m"

    # Run the xuc setup script on XiVO PBX
    # shellcheck disable=SC2029
    ssh "root@${xivo_host}" "bash $XIVOCC_CONFIG_SCRIPT \
        -h $XUC_HOST \
        -r $RESTART_REPLY \
        -s $XIVO_AMI_SECRET \
        -t $INSTALLATION_TYPE"
}

is_custom_env_variable_in_xivocc() {
    variable_name="${1}"
    grep -oP -m 1 "^\s*\K${variable_name}\s*=" ${COMPOSE_PATH}/${CUSTOM_ENV_FILE} >/dev/null
    return $?
}

copy_custom_env_variable_from_xivo_to_xivocc() {
    local xivo_host="${1}"
    local variable_name="${2}"
    local get_variable_from_xivo
    local get_variable_from_xivocc

    # shellcheck disable=SC2029
    get_variable_from_xivo=$(ssh "root@${xivo_host}" "grep -oP -m 1 '^\s*${variable_name}\s*=\s*\K.*$' /etc/docker/xivo/custom.env" | sed -e 's/[[:space:]]*$//')
    if [ -z "${get_variable_from_xivo}" ]; then
        echo -e "\\e[1;33mWARN: ${variable_name} was not found in XiVO PBX configuration\\e[0m"
        return 1
    fi

    if is_custom_env_variable_in_xivocc "${variable_name}"; then
        get_variable_from_xivocc=$(grep -oP -m 1 "^\s*${variable_name}\s*=\s*\K.*$" ${COMPOSE_PATH}/${CUSTOM_ENV_FILE})
        if [ "${get_variable_from_xivo}" = "${get_variable_from_xivocc}" ]; then
            echo -e "${variable_name} is already set on both xivo and xivocc, nothing to do"
            return 0
        else
            sed -i "s/^\s*${variable_name}\s*=.*/${variable_name}=${get_variable_from_xivo}/" ${COMPOSE_PATH}/${CUSTOM_ENV_FILE}
            echo -e "${variable_name} successfully edited in custom.env"
        fi
    else
        echo "${variable_name}=${get_variable_from_xivo}" >>${COMPOSE_PATH}/${CUSTOM_ENV_FILE}
        echo -e "${variable_name} successfully added to custom.env"
    fi
}

xivocc_configures_xivo_post_wizard() {
    if check_if_xivo_is_ssh_reachable "${XIVO_HOST}"; then
        if check_if_xivo_configured "${XIVO_HOST}"; then
            install_package_on_xivo "${XIVO_HOST}"
            launch_xivo_configuration_script "${XIVO_HOST}"
            copy_custom_env_variable_from_xivo_to_xivocc "${XIVO_HOST}" "FLUENTD_SECRET"
        fi
    fi
}

#!/usr/bin/env bash

if [ -z "$1" ]; then
	exit 1
fi

status=$(docker inspect --format="{{.State.Health.Status}}" xivo-"$1"-1 2>/dev/null)

if [ "$status" = "healthy" ]; then
    exit 0
elif [ "$status" = "unhealthy" ]; then
    exit 3
else
    exit 1
fi

#!/bin/bash

set -e

is_xivoucaddon() {
    [ -f /var/lib/xivo/uc_enabled ]
}

_run_psql() {
  PGPASSWORD=${PGPASSWORD} psql -h $DB_HOST -p $DB_PORT -U $DB_USERNAME -t -c "$1"
}

is_postgres_running() {
  result=$(PGPASSWORD=${PGPASSWORD} psql -h $DB_HOST -p $DB_PORT -U $DB_USERNAME -qAtc "select 1 from pg_catalog.pg_database WHERE datname='postgres'" 2>/dev/null)

  [ $? -eq 0 ] && [ "$result" = "1" ]
}

wait_for_postgres() {
  local wait_time=120
  echo -n "Waiting $wait_time seconds for database ready at $DB_HOST:$DB_PORT..."

  local db_running="false"
  for _ in $(seq 1 $wait_time); do
    if is_postgres_running; then
      db_running="true"
      break
    else
      sleep 1
      echo -n "."
    fi
  done
  echo
  if [ "$db_running" = "false" ]; then
    echo "ERROR: database is not running."
    exit 1
  fi
}

get_var_from_environment() {
  if [[ -z "${MMUSER_DB_PASSWORD}" ]]; then
    echo "MMUSER_DB_PASSWORD environment variable not set, unable to create user, exiting with error"
    exit 1
  fi
}

create_db_and_user() {
  DB_EXISTS=$(_run_psql "SELECT 1 FROM pg_database WHERE datname='mattermost'")
  if [[ -z "${DB_EXISTS}" ]]; then
    _run_psql "CREATE DATABASE mattermost"
  fi

  USER_EXISTS=$(_run_psql "SELECT 1 FROM pg_roles WHERE rolname='mmuser'")
  if [[ -z "${USER_EXISTS}" ]]; then
    _run_psql "CREATE USER mmuser LOGIN PASSWORD '${MMUSER_DB_PASSWORD}'"
    _run_psql "ALTER DATABASE mattermost OWNER TO mmuser"
  else
    _run_psql "ALTER USER mmuser PASSWORD '${MMUSER_DB_PASSWORD}'"
  fi
}

# Script begins here

DB_USERNAME=postgres
if is_xivoucaddon; then
  DB_PORT=5432
  PGPASSWORD=proformatique
else
  DB_PORT=5443
  PGPASSWORD=xivocc
fi

while getopts h:w: opt; do
  case "${opt}" in
  h) DB_HOST=${OPTARG} ;;
  w) MMUSER_DB_PASSWORD=${OPTARG} ;;
  *)
    echo "Missing arguments."
    exit 1
    ;;
  esac
done

wait_for_postgres
get_var_from_environment
create_db_and_user

echo "$0 update finished, mattermost db and user created"

#!/bin/bash
# preinst script for xivouc
#
# see: dh_installdeb(1)

set -e

COMPOSE_PATH="/etc/docker/compose"
COMPOSE_FILE="docker-xivocc.yml"
FACTORY_ENV_FILE="factory.env"

check_compose_installed() {
    if [ ! -e /usr/libexec/docker/cli-plugins/docker-compose ] ; then
        echo "Docker Compose is not installed."
        echo "Please run xivouc-installer to install XiVO UC."
        exit 2
    fi
}

case "$1" in
    install)
        check_compose_installed
    ;;

    upgrade)
        current_version=$2
        if [[ ${current_version} < "2018.05" ]]; then
            # We need to remove docker bridge to update its network range
            # (which will be done in postinst)
            echo "Stopping xivouc services..."
            cd ${COMPOSE_PATH}
            docker compose -p xivocc -f ${COMPOSE_PATH}/${COMPOSE_FILE} stop
            if docker network ls | grep xivocc_default ; then
                echo "Removing old xivocc_default docker network..."
                docker network rm xivocc_default
            fi
            # Then remove correct line from pg_hba.conf
            OLD_DOCKER_NET=$(grep -oP -m 1 '^\s*DOCKER_NET=\K.*' ${COMPOSE_PATH}/${FACTORY_ENV_FILE})
            OLD_DOCKER_NET_SED_READY=$(echo ${OLD_DOCKER_NET}|sed -e 's#/#\\/#' -e 's#\.#\\.#g')
            sed -i.bak "/host\s*asterisk\s*all\s*${OLD_DOCKER_NET_SED_READY}\s*md5/d" /etc/postgresql/9.4/main/pg_hba.conf
        fi
    ;;

    abort-upgrade)
    ;;

    *)
        echo "preinst called with unknown argument \`$1'" >&2
        exit 1
    ;;
esac

# dh_installdeb will replace this with shell code automatically
# generated by other debhelper scripts.

#DEBHELPER#

exit 0

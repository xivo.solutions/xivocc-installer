#!/bin/bash
# postinst script for xivo docker components
#
# see: dh_installdeb(1)

set -e

COMPOSE_PATH="/etc/docker/xivo"
COMPOSE_FILE="docker-xivo.yml"
ENV_FILE=".env"
FACTORY_ENV_FILE="factory.env"
CUSTOM_ENV_FILE="custom.env"

HEADER="# This is an auto-generated file, please customize your configuration in the $COMPOSE_PATH/$CUSTOM_ENV_FILE file"

RESTART_REPLY=false
PLAY_AUTH_TOKEN=sreSgq6BiEzKR80f03WtFOyzXirnJlzsaUXwkQhHsBReaVj5UJal5323dY6GBtRK

create_nginx_certificate() {
    local ssldir
    ssldir=/etc/docker/nginx/ssl
    mkdir -p "${ssldir}"
    openssl req -nodes -newkey rsa:2048 -keyout "${ssldir}/xivoxc.key" -out "${ssldir}/xivoxc.csr" -subj "/C=FR/ST=Rhone-Alpes/L=Limonest/O=Avencall/CN=$(hostname --fqdn)"
    openssl x509 -req -days 3650 -in "${ssldir}/xivoxc.csr" -signkey "${ssldir}/xivoxc.key" -out "${ssldir}/xivoxc.crt"
}

create_secret() {
    local secret_length="${1}"; shift
    local generated_secret

    generated_secret=$(< /dev/urandom tr -dc A-Za-z0-9 | head -c "${secret_length}";)
    echo "${generated_secret}"
}

remove_key_in_custom_env() {
    local key_to_remove="${1}"; shift

    sed -e "/^${key_to_remove}/d" -i ${COMPOSE_PATH}/${CUSTOM_ENV_FILE}
}

set_configmgt_auth_secret_in_custom_env_file() {
    local configmgt_auth_secret
    configmgt_auth_secret=$(create_secret "64")
    echo "CONFIGMGT_AUTH_SECRET=${configmgt_auth_secret}" >> ${COMPOSE_PATH}/${CUSTOM_ENV_FILE}
}

set_play_secret_in_custom_env_file() {
    local play_secret
    play_secret=$(create_secret "64")
    echo "APPLICATION_SECRET=${play_secret}" >> ${COMPOSE_PATH}/${CUSTOM_ENV_FILE}
}

set_play_auth_token_in_custom_env_file() {
    echo "PLAY_AUTH_TOKEN=${PLAY_AUTH_TOKEN}" >> ${COMPOSE_PATH}/${CUSTOM_ENV_FILE}
}

set_usm_db_access_in_custom_env() {
    local usm_event_secret
    usm_event_secret=$(create_secret "64")
    echo "USM_EVENT_SECRET=${usm_event_secret}" >> ${COMPOSE_PATH}/${CUSTOM_ENV_FILE}

    local usm_config_secret
    usm_config_secret=$(create_secret "64")
    echo "USM_CONFIG_SECRET=${usm_config_secret}" >> ${COMPOSE_PATH}/${CUSTOM_ENV_FILE}

    local usm_collector_secret
    usm_collector_secret=$(create_secret "64")
    echo "USM_COLLECTOR_SECRET=${usm_collector_secret}" >> ${COMPOSE_PATH}/${CUSTOM_ENV_FILE}
}

set_usm_rabbitmq_access_in_custom_env() {
    local usm_rabbitmq_secret
    usm_rabbitmq_secret=$(create_secret "64")
    echo "USM_EVENT_AMQP_SECRET=${usm_rabbitmq_secret}" >> ${COMPOSE_PATH}/${CUSTOM_ENV_FILE}
}

remove_old_rabbitmq_host_user() {
    rm -rf /var/log/rabbitmq \
    /etc/rabbitmq \
    /var/lib/rabbitmq
    userdel rabbitmq
}

set_db_access_for_call_quality_stats_components_in_custom_env() {
    local grafana_secret
    grafana_secret=$(create_secret "64")
    echo "GRAFANA_SECRET=${grafana_secret}" >> ${COMPOSE_PATH}/${CUSTOM_ENV_FILE}
    local fluentd_secret
    fluentd_secret=$(create_secret "64")
    echo "FLUENTD_SECRET=${fluentd_secret}" >> ${COMPOSE_PATH}/${CUSTOM_ENV_FILE}
}

generate_custom_env_file() {
    echo "Generating ${CUSTOM_ENV_FILE} configuration file"
    DOCKER_BRIDGE_IP=$(ip addr show docker0 | grep "inet\b" | awk '{print $2}' | cut -d/ -f1)
    echo "XIVO_HOST=${DOCKER_BRIDGE_IP}" > ${COMPOSE_PATH}/${CUSTOM_ENV_FILE}
    echo "DB_HOST=${DOCKER_BRIDGE_IP}" >> ${COMPOSE_PATH}/${CUSTOM_ENV_FILE}
    set_play_secret_in_custom_env_file
	set_play_auth_token_in_custom_env_file
    set_configmgt_auth_secret_in_custom_env_file
    set_usm_db_access_in_custom_env    
    set_usm_rabbitmq_access_in_custom_env
    set_db_access_for_call_quality_stats_components_in_custom_env
}

get_ari_password() {
python3 << END
import sys, configparser

config = configparser.RawConfigParser()
config.read("/etc/asterisk/ari.conf")

try:
    password = config.get("xivo", "password")
except:
    password = ""

print(password)
END
}

install_outcall_application() {
    mkdir -p /var/log/xivo-outcall
    chown -R daemon:daemon /var/log/xivo-outcall

    echo "Getting ARI password"
    ARI_PASSWORD=$(get_ari_password)
    if [ -z "$ARI_PASSWORD" ]; then
        echo -e '\e[1;31mPassword for ARI user "xivo" was not found in ari.conf\e[0m'
        echo -e '\e[1;31mYou must fix ARI configuration in custom.env\e[0m'
        echo "ARI_USERNAME=" >> ${COMPOSE_PATH}/${CUSTOM_ENV_FILE}
    fi
    echo "ARI_PASSWORD=${ARI_PASSWORD}" >> ${COMPOSE_PATH}/${CUSTOM_ENV_FILE}
}

install_dockerized_webi() {
    WEBI_LOG="/var/log/xivo-web-interface"
    mkdir -p $WEBI_LOG
    touch $WEBI_LOG/xivo.log
    chown -R www-data:www-data $WEBI_LOG
}

install_xivo_switchboard_stats() {
    WEBI_LOG="/var/log/xivo-switchboard-reports"
    mkdir -p $WEBI_LOG
    touch $WEBI_LOG/xivo-switchboard-reports.log
    chown -R daemon:daemon $WEBI_LOG
}

case "$1" in
    configure)
        if [ -z "$2" ]; then
            # This pkg replaces xivo-configmgt pkg. So we verify if xivo-configmgt was installed.
            # If yes we need to skip fresh install.
            configmgt_pkg_status=$(dpkg-query -W -f='${Status}' xivo-configmgt || echo "ERROR")
            configmgt_pkg_was_installed="x"
            if [[ "$configmgt_pkg_status" == "ERROR" || "$configmgt_pkg_status" =~ "not-installed" ]]; then
                configmgt_pkg_was_installed="false"
            fi
            if [ "$configmgt_pkg_was_installed" == "false" ]; then
                # New XiVO installation
                echo "XiVO with Docker Components Configuration..."

                # Generic XiVO UC config
                mkdir -p ${COMPOSE_PATH}

                generate_custom_env_file

                mkdir -p /var/log/xivo-configmgt
                chown -R daemon:daemon /var/log/xivo-configmgt
            fi
            # New installation steps after xivo-configmgt replacement
            # (upgrade from < 2018.13 or new XiVO installation)
            install_outcall_application
            install_dockerized_webi
            install_xivo_switchboard_stats
            create_nginx_certificate
        else
            previous_version="$2"
            # Here goes the upgrade of old package

            if dpkg --compare-versions "$previous_version" "<<" "2019.02.00"; then
                set_play_secret_in_custom_env_file
                install_outcall_application
            fi
            if dpkg --compare-versions "$previous_version" "<<" "2019.10.00"; then
                install_dockerized_webi
            fi
            if dpkg --compare-versions "$previous_version" "<<" "2020.03.00"; then
                install_xivo_switchboard_stats
            fi

            if dpkg --compare-versions "$previous_version" "<<" "2020.20.00"; then
                set_play_auth_token_in_custom_env_file
            fi

            if dpkg --compare-versions "$previous_version" "<<" "2021.15.00"; then
                if ! grep -q CONFIGMGT_AUTH_SECRET ${COMPOSE_PATH}/${CUSTOM_ENV_FILE}; then
                    set_configmgt_auth_secret_in_custom_env_file
                fi
            fi

            if dpkg --compare-versions "$previous_version" "<<" "2021.15.01"; then
                # Fix pwd length generated for Helios.00
                if dpkg --compare-versions "$previous_version" ">=" "2021.15.00"; then
                    if grep -q CONFIGMGT_AUTH_SECRET ${COMPOSE_PATH}/${CUSTOM_ENV_FILE}; then
                        remove_key_in_custom_env "CONFIGMGT_AUTH_SECRET"
                        set_configmgt_auth_secret_in_custom_env_file
                    fi
                fi
                # Add default selfsigned certificate for xivo nginx
                create_nginx_certificate
            fi

            if dpkg --compare-versions "$previous_version" "<<" "2022.10.02"; then
                set_usm_db_access_in_custom_env
                set_usm_rabbitmq_access_in_custom_env
                remove_old_rabbitmq_host_user
            fi

            if dpkg --compare-versions "$previous_version" "<<" "2023.05.00"; then
                set_db_access_for_call_quality_stats_components_in_custom_env
            fi


            echo -e "\e[1;33mPlease download new images with command xivo-dcomp pull\e[0m"
            echo -e "\e[1;33mAnd start new containers with command xivo-dcomp up -d --remove-orphans\e[0m"
        fi;
    ;;
    abort-upgrade|abort-remove|abort-deconfigure)
    ;;
    *)
        echo "postinst called with unknown argument '$1'" >&2
        exit 1
    ;;
esac
# dh_installdeb will replace this with shell code automatically
# generated by other debhelper scripts.
#DEBHELPER#
exit 0
